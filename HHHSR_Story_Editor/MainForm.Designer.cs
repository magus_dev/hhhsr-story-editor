﻿namespace HHHSR_Story_Editor
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonOpenStory = new System.Windows.Forms.Button();
            this.listBoxStoryEvents = new System.Windows.Forms.ListBox();
            this.pictureBoxTifan = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.buttonPreviousStoryEvent = new System.Windows.Forms.Button();
            this.buttonNextStoryEvent = new System.Windows.Forms.Button();
            this.buttonAddStoryEvent = new System.Windows.Forms.Button();
            this.panelEventEditor = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTifan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.Panel1.SuspendLayout();
            this.splitContainer9.Panel2.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenStory
            // 
            this.buttonOpenStory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOpenStory.Location = new System.Drawing.Point(0, 0);
            this.buttonOpenStory.Name = "buttonOpenStory";
            this.buttonOpenStory.Size = new System.Drawing.Size(99, 31);
            this.buttonOpenStory.TabIndex = 2;
            this.buttonOpenStory.Text = "Open story";
            this.buttonOpenStory.UseVisualStyleBackColor = true;
            this.buttonOpenStory.Click += new System.EventHandler(this.buttonOpenStory_Click);
            // 
            // listBoxStoryEvents
            // 
            this.listBoxStoryEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxStoryEvents.FormattingEnabled = true;
            this.listBoxStoryEvents.Location = new System.Drawing.Point(0, 0);
            this.listBoxStoryEvents.Name = "listBoxStoryEvents";
            this.listBoxStoryEvents.Size = new System.Drawing.Size(286, 466);
            this.listBoxStoryEvents.TabIndex = 3;
            this.listBoxStoryEvents.SelectedIndexChanged += new System.EventHandler(this.listBoxStoryEvents_SelectedIndexChanged);
            // 
            // pictureBoxTifan
            // 
            this.pictureBoxTifan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTifan.BackgroundImage")));
            this.pictureBoxTifan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxTifan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxTifan.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTifan.InitialImage")));
            this.pictureBoxTifan.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxTifan.Name = "pictureBoxTifan";
            this.pictureBoxTifan.Size = new System.Drawing.Size(56, 89);
            this.pictureBoxTifan.TabIndex = 4;
            this.pictureBoxTifan.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(951, 603);
            this.splitContainer1.SplitterDistance = 559;
            this.splitContainer1.TabIndex = 5;
            this.splitContainer1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panelEventEditor);
            this.splitContainer2.Size = new System.Drawing.Size(951, 559);
            this.splitContainer2.SplitterDistance = 317;
            this.splitContainer2.TabIndex = 5;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(317, 559);
            this.splitContainer3.SplitterDistance = 27;
            this.splitContainer3.TabIndex = 0;
            this.splitContainer3.TabStop = false;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.listBoxStoryEvents);
            this.splitContainer4.Size = new System.Drawing.Size(286, 559);
            this.splitContainer4.SplitterDistance = 89;
            this.splitContainer4.TabIndex = 0;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.pictureBoxTifan);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(286, 89);
            this.splitContainer5.SplitterDistance = 56;
            this.splitContainer5.TabIndex = 0;
            this.splitContainer5.TabStop = false;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.splitContainer7);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.splitContainer8);
            this.splitContainer6.Size = new System.Drawing.Size(226, 89);
            this.splitContainer6.SplitterDistance = 99;
            this.splitContainer6.TabIndex = 0;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.buttonOpenStory);
            this.splitContainer7.Size = new System.Drawing.Size(99, 89);
            this.splitContainer7.SplitterDistance = 54;
            this.splitContainer7.TabIndex = 0;
            this.splitContainer7.TabStop = false;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.splitContainer9);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.buttonAddStoryEvent);
            this.splitContainer8.Size = new System.Drawing.Size(123, 89);
            this.splitContainer8.SplitterDistance = 55;
            this.splitContainer8.TabIndex = 0;
            this.splitContainer8.TabStop = false;
            // 
            // splitContainer9
            // 
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.Location = new System.Drawing.Point(0, 0);
            this.splitContainer9.Name = "splitContainer9";
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.Controls.Add(this.buttonPreviousStoryEvent);
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.Controls.Add(this.buttonNextStoryEvent);
            this.splitContainer9.Size = new System.Drawing.Size(123, 55);
            this.splitContainer9.SplitterDistance = 57;
            this.splitContainer9.TabIndex = 0;
            this.splitContainer9.TabStop = false;
            // 
            // buttonPreviousStoryEvent
            // 
            this.buttonPreviousStoryEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPreviousStoryEvent.Location = new System.Drawing.Point(0, 0);
            this.buttonPreviousStoryEvent.Name = "buttonPreviousStoryEvent";
            this.buttonPreviousStoryEvent.Size = new System.Drawing.Size(57, 55);
            this.buttonPreviousStoryEvent.TabIndex = 0;
            this.buttonPreviousStoryEvent.Text = "<";
            this.buttonPreviousStoryEvent.UseVisualStyleBackColor = true;
            this.buttonPreviousStoryEvent.Click += new System.EventHandler(this.buttonPreviousStoryEvent_Click);
            // 
            // buttonNextStoryEvent
            // 
            this.buttonNextStoryEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonNextStoryEvent.Location = new System.Drawing.Point(0, 0);
            this.buttonNextStoryEvent.Name = "buttonNextStoryEvent";
            this.buttonNextStoryEvent.Size = new System.Drawing.Size(62, 55);
            this.buttonNextStoryEvent.TabIndex = 0;
            this.buttonNextStoryEvent.Text = ">";
            this.buttonNextStoryEvent.UseVisualStyleBackColor = true;
            this.buttonNextStoryEvent.Click += new System.EventHandler(this.buttonNextStoryEvent_Click);
            // 
            // buttonAddStoryEvent
            // 
            this.buttonAddStoryEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddStoryEvent.Location = new System.Drawing.Point(0, 0);
            this.buttonAddStoryEvent.Name = "buttonAddStoryEvent";
            this.buttonAddStoryEvent.Size = new System.Drawing.Size(123, 30);
            this.buttonAddStoryEvent.TabIndex = 0;
            this.buttonAddStoryEvent.Text = "Add story event";
            this.buttonAddStoryEvent.UseVisualStyleBackColor = true;
            // 
            // panelEventEditor
            // 
            this.panelEventEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEventEditor.Location = new System.Drawing.Point(0, 0);
            this.panelEventEditor.Name = "panelEventEditor";
            this.panelEventEditor.Size = new System.Drawing.Size(630, 559);
            this.panelEventEditor.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 603);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "HHHSR Story Editor";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTifan)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
            this.splitContainer8.ResumeLayout(false);
            this.splitContainer9.Panel1.ResumeLayout(false);
            this.splitContainer9.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonOpenStory;
        private System.Windows.Forms.ListBox listBoxStoryEvents;
        private System.Windows.Forms.PictureBox pictureBoxTifan;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.SplitContainer splitContainer8;
        private System.Windows.Forms.Panel panelEventEditor;
        private System.Windows.Forms.Button buttonAddStoryEvent;
        private System.Windows.Forms.SplitContainer splitContainer9;
        private System.Windows.Forms.Button buttonPreviousStoryEvent;
        private System.Windows.Forms.Button buttonNextStoryEvent;
    }
}

