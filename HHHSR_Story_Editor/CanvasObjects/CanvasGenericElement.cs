﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.CanvasObjects
{
    class CanvasGenericElement : CanvasElement
    {
        public CanvasGenericElement(string name, float x, float y, float width, float height) : base(name, x, y, width, height, Type.Generic)
        {
        }
    }
}
