﻿using HHHSR_Story_Editor.Story.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HHHSR_Story_Editor
{
    public partial class StoryEntryEventEditorForm : Form
    {
        public StoryEntryEventEditorForm()
        {
            InitializeComponent();
        }

        public static void SetData(StoryEntryEventEditorForm eventEditorForm, StoryEntryEvent evt)
        {
            if (evt == null || eventEditorForm == null)
            {
                return;
            }

            eventEditorForm.textBoxEventID.Text = evt.ID.ToString();
            eventEditorForm.textBoxEntryEventID.Text = evt.NEXT_ID.ToString();


        }

        private void buttonGotoNextEvent_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.GotoNextEvent(ulong.Parse(this.textBoxEntryEventID.Text));
            }catch(Exception ex)
            {
                MessageBox.Show("The next ID does not exist or could not be parsed!");
            }
        }
    }
}
