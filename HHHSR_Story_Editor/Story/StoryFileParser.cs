﻿using HHHSR_Story_Editor.Story.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.Story
{
    class StoryFileParser
    {
        private const int MIN_LINES_PER_FILE = 3;
        private enum MetaInfoLines
        {
            MagicNumber,
            Id,
            Type,

        }
        public static readonly string STORY_FILE_MAGIC_NUMBER_STRING = "Studio CxD\r\n";
        public static readonly byte[] STORY_FILE_MAGIC_NUMBER = new byte[]
        {
            0x53, 0x74, 0x75, 0x64, 0x69, 0x6F, 0x20, 0x43, 0x78, 0x44, 0x0D, 0x0A
        };


        public static StoryEvent ParseFile(string storyFile)
        {
            if (string.IsNullOrEmpty(storyFile))
            {
                return null;
            }

            byte[] magicNumberCheckBuffer = new byte[STORY_FILE_MAGIC_NUMBER.Length + 1];
            using (FileStream fs = new FileStream(storyFile, FileMode.Open, FileAccess.Read))
            {
                fs.Read(magicNumberCheckBuffer, 0, magicNumberCheckBuffer.Length);
                fs.Close();
            }
            for (int i = 0; i < STORY_FILE_MAGIC_NUMBER.Length; i++)
            {
                if (magicNumberCheckBuffer[i] != STORY_FILE_MAGIC_NUMBER[i])
                {
                    return null;
                }
            }

            string content = File.ReadAllText(storyFile);
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }

            string[] storyDatasArray = content.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            if(storyDatasArray == null || storyDatasArray.Length < MIN_LINES_PER_FILE) 
            {
                return null;
            }

            List<string> storyDatas = new List<string>(storyDatasArray);

            ulong ID = ulong.MaxValue;
            string idString = storyDatas[(int)MetaInfoLines.Id];
            try
            {
                if(!ulong.TryParse(idString, out ID))
                {
                    return null;
                }
            }catch(Exception ex)
            {
                Console.WriteLine("ERROR : Could not parse ID line : " + idString + "  in file : " + storyFile );
            }

            ulong TYPE = ulong.MaxValue;
            string typeString = storyDatas[(int)MetaInfoLines.Type];
            try
            {
                if (!ulong.TryParse(typeString, out TYPE))
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR : Could not parse TYPE line : " + typeString + "  in file : " + storyFile);
            }

            if(!Enum.IsDefined(typeof(StoryEvent.StoryEventTypes), (int)TYPE)){
                return null;
            }
            StoryEvent.StoryEventTypes type = (StoryEvent.StoryEventTypes)(int)TYPE;

            storyDatas.RemoveAt(0);
            storyDatas.RemoveAt(0);
            storyDatas.RemoveAt(0);

            switch (type)
            {
                case StoryEvent.StoryEventTypes.EntryPoint:
                    return StoryEntryEvent.Create(storyDatas, ID);
                case StoryEvent.StoryEventTypes.Dialogue:
                    return DialogueEvent.Create(storyDatas, ID);
                case StoryEvent.StoryEventTypes.Rub:

                    break;
                case StoryEvent.StoryEventTypes.Slide:

                    break;
            }


            return null;
        }

    }
}
