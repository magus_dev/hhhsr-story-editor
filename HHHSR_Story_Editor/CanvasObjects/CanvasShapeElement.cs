﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.CanvasObjects
{
    class CanvasShapeElement : CanvasElement
    {
        public enum Shape
        {
            Ellipse,
            Rectangle,
        }

        public Shape shape;

        public CanvasShapeElement(string name, float x, float y, float width, float height, Shape shape) : base(name, x, y, width, height, Type.Shape)
        {
            this.shape = shape;
        }
    }
}
