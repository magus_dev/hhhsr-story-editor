﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.CanvasObjects
{
    public class CanvasImageElement : CanvasElement
    {
        public Image image;
        public CanvasImageElement(string name, float x, float y, float width, float height, Image image) : base(name, x, y, width, height, Type.Image)
        {
            this.image = image;
        }


    }
}
