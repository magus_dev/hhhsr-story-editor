﻿using HHHSR_Story_Editor.Story.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.Story
{
    class Story
    {
        public ulong storyEntryId = 0;
        public Dictionary<ulong, string> storyFiles = new Dictionary<ulong, string>();
        public Dictionary<ulong, StoryEvent> storyEvents = new Dictionary<ulong, StoryEvent>();
    }
}
