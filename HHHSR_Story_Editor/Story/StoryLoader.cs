﻿using HHHSR_Story_Editor.Story.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.Story
{
    class StoryLoader
    {

        public StoryLoader()
        {
            Initialize();
        }

        private void Initialize()
        {
        }

        public Story OpenFolder(string path)
        {
            Story bestStory = new Story();
            // Enumerate all (story)files inside this directory and subdirectories
            var fileEnum = Directory.EnumerateFiles(path, "*" , SearchOption.AllDirectories);
            

            foreach (var fileEntry in fileEnum)
            {
                if (string.IsNullOrEmpty(fileEntry))
                {
                    continue;
                }

                // Try to load the event from the file 
                StoryEvent evt = StoryFileParser.ParseFile(fileEntry);
                if(evt == null)
                {
                    Console.WriteLine("ERROR : Could not load story event from file : " + fileEntry);
                    continue;
                }
                // and put them in the dictionaries
                bestStory.storyFiles.Add(evt.ID, fileEntry);
                bestStory.storyEvents.Add(evt.ID, evt);

                // This is to find the story entry point !
                if (evt is StoryEntryEvent)
                {
                    bestStory.storyEntryId = evt.ID;
                }
                
            }

            return bestStory;

            return null;
        }
        
    }
}
