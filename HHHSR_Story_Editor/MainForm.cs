﻿using HHHSR_Story_Editor.Story;
using HHHSR_Story_Editor.Story.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HHHSR_Story_Editor
{
    public partial class MainForm : Form
    {
        public static MainForm startForm;
        public static string lastFolderOpened = "";
        public static string storyRootFolder = "";
        Story.Story story;

        List<ulong> storyIds;
        List<ulong> storyNextHistory;
        bool eventChangeByButton = false;
        StoryEvent currentEvent;

        Form eventEditorForm;


        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public MainForm()
        {
            storyNextHistory = new List<ulong>();
            startForm = this;
            InitializeComponent();
            // Faster resizing :
            this.ResizeBegin += (s, e) => { this.SuspendLayout(); };
            this.ResizeEnd += (s, e) => { this.ResumeLayout(true); };
            //this.FormBorderStyle = FormBorderStyle.;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(1, 1, Width - 1, Height - 1, 12, 12));
        }

        private void buttonOpenStory_Click(object sender, EventArgs e)
        {
            buttonOpenStory.Enabled = false;
            string lpath = "last_story_path.txt";
            StoryLoader s = new StoryLoader();
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "Just select the story folder.";
            folderBrowserDialog.ShowNewFolderButton = false;
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
            try
            {
                if (File.Exists(lpath))
                {
                    string path = File.ReadAllText(lpath);
                    if (!string.IsNullOrEmpty(path))
                    {
                        folderBrowserDialog.SelectedPath = path;
                    }
                }
                
            }
            catch(Exception ex)
            {

            }
            DialogResult result = folderBrowserDialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                lastFolderOpened = folderBrowserDialog.SelectedPath;
                storyRootFolder = Directory.GetParent(lastFolderOpened).FullName;
                try
                {
                    if (File.Exists(lpath))
                    {
                        File.Delete(lpath);
                    }
                    File.WriteAllText(lpath, folderBrowserDialog.SelectedPath);

                }
                catch (Exception ex)
                {

                }
                storyIds = new List<ulong>();
                story = s.OpenFolder(folderBrowserDialog.SelectedPath);
                this.Text = folderBrowserDialog.SelectedPath + " - HHHSR Story Editor";
                FillUI();
            }
            else
            {
                buttonOpenStory.Enabled = true;
            }
        }

        private void FillUI()
        {
            listBoxStoryEvents.Items.Clear();
            //var ienum = story.storyFiles.GetEnumerator();
            foreach (var evt in story.storyEvents)
            {
                if(evt.Value is StoryEntryEvent)
                {
                    listBoxStoryEvents.Items.Insert(0,
                        Path.GetFileName(story.storyFiles[evt.Key])
                        );
                    storyIds.Insert(0, evt.Key);
                }
                else
                {
                    listBoxStoryEvents.Items.Add(
                        Path.GetFileName(story.storyFiles[evt.Key])
                        );
                    storyIds.Add(evt.Key);
                }

            }
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBoxStoryEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBoxStoryEvents.SelectedIndex;
            if(index < 0)
            {
                return;
            }
            if(index >= storyIds.Count)
            {
                return;
            }
            ulong evtId = storyIds[index];
            OpenEvent(evtId);
        }

        public void OpenEvent(ulong NEXT_ID)
        {
            if (!story.storyEvents.ContainsKey(NEXT_ID))
            {
                return;
            }

            StoryEvent evt = story.storyEvents[NEXT_ID];
            if (evt == null)
            {
                return;
            }

            currentEvent = evt;

            if (eventEditorForm != null)
            {
                eventEditorForm.Dispose();
                eventEditorForm = null;
            }

            panelEventEditor.Controls.Clear();

            if (evt is StoryEntryEvent)
            {
                eventEditorForm = new StoryEntryEventEditorForm();
                eventEditorForm.TopLevel = false;
                panelEventEditor.Controls.Add(eventEditorForm);
                eventEditorForm.FormBorderStyle = FormBorderStyle.None;
                eventEditorForm.Dock = DockStyle.Fill;
                eventEditorForm.Show();
                StoryEntryEventEditorForm.SetData((StoryEntryEventEditorForm)eventEditorForm, (StoryEntryEvent)evt);
            }
            else if (evt is DialogueEvent)
            {

                eventEditorForm = new DialogueEventEditorForm();
                eventEditorForm.TopLevel = false;
                panelEventEditor.Controls.Add(eventEditorForm);
                eventEditorForm.FormBorderStyle = FormBorderStyle.None;
                eventEditorForm.Dock = DockStyle.Fill;
                eventEditorForm.Show();
                DialogueEventEditorForm.SetData((DialogueEventEditorForm)eventEditorForm, (DialogueEvent)evt, OnDialogueEventChanged);
            }
        }

        private void OnDialogueEventChanged(DialogueEvent obj)
        {
            if(obj == null)
            {
                return;
            }
            if (story.storyEvents.ContainsKey(obj.ID))
            {
                if(story.storyEvents[obj.ID] is DialogueEvent)
                {
                    ((DialogueEvent)story.storyEvents[obj.ID]).BACKGROUND_NAME = obj.BACKGROUND_NAME;
                    ((DialogueEvent)story.storyEvents[obj.ID]).NEXT_ID = obj.NEXT_ID;
                    ((DialogueEvent)story.storyEvents[obj.ID]).TEXT = obj.TEXT;

                    DialogueEvent.SaveFile(story.storyFiles[obj.ID], ((DialogueEvent)story.storyEvents[obj.ID]));
                }
            }
        }

        public static void GotoPreviousEvent()
        {
            if(startForm.storyNextHistory.Count < 1)
            {
                return;
            }
            ulong PREV_ID = startForm.storyNextHistory.Last();
            startForm.storyNextHistory.RemoveAt(startForm.storyNextHistory.Count - 1);
            int index = 0;
            foreach (var id in startForm.storyIds)
            {
                if (PREV_ID == id)
                {
                    startForm.listBoxStoryEvents.SelectedIndex = index;
                    //startForm.OpenEvent(NEXT_ID);
                    return;
                }
                index++;
            }
        }

        public static void GotoNextEvent(ulong NEXT_ID) 
        {
            int index = 0;
            foreach (var id in startForm.storyIds)
            {
                if(NEXT_ID == id)
                {
                    startForm.storyNextHistory.Add(startForm.currentEvent.ID);
                    startForm.listBoxStoryEvents.SelectedIndex = index;
                    //startForm.OpenEvent(NEXT_ID);
                    return;
                }
                index++;
            }
        }

        private void buttonPreviousStoryEvent_Click(object sender, EventArgs e)
        {
            eventChangeByButton = true;
            GotoPreviousEvent();
            eventChangeByButton = false;
        }

        private void buttonNextStoryEvent_Click(object sender, EventArgs e)
        {
            eventChangeByButton = true;
            if(currentEvent != null)
            {
                GotoNextEvent(currentEvent.NEXT_ID);
            }
            eventChangeByButton = false;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {

            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(1, 1, Width - 1, Height - 1, 12, 12));
        }
    }
}
