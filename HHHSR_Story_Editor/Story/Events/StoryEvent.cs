﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.Story.Events
{
    public class StoryEvent
    {
        public ulong ID;
        public ulong NEXT_ID;

        public enum StoryEventTypes
        {
            EntryPoint,
            Dialogue,
            Rub,
            Slide,

        }

        public virtual ulong GetNextID()
        {
            return NEXT_ID;
        }

    }
}
