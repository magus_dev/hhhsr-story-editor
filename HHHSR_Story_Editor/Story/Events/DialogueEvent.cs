﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.Story.Events
{
    public class DialogueEvent : StoryEvent
    {
        public string TEXT;
        public string BACKGROUND_NAME;

        public static DialogueEvent Create(List<string> storyDatas, ulong ID)
        {
            if (storyDatas == null || storyDatas.Count < 3)
            {
                return null;
            }

            DialogueEvent dlg = new DialogueEvent();
            ulong NEXT_ID = ulong.MaxValue;
            try
            {
                if (!ulong.TryParse(storyDatas[0], out NEXT_ID))
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Failed to parse NEXT_ID in DialogueEvent");
            }

            dlg.ID = ID;
            dlg.NEXT_ID = NEXT_ID;
            dlg.TEXT = storyDatas[1];
            dlg.BACKGROUND_NAME = storyDatas[2];
            return dlg;
        }

        public static void SaveFile(string filename, DialogueEvent evt)
        {
            File.WriteAllText(filename, StoryFileParser.STORY_FILE_MAGIC_NUMBER_STRING);
            File.AppendAllText(filename, evt.ID.ToString() + "\r\n");
            File.AppendAllText(filename, ((int)StoryEventTypes.Dialogue).ToString() + "\r\n");
            File.AppendAllText(filename, evt.NEXT_ID.ToString() + "\r\n");
            File.AppendAllText(filename, evt.TEXT + "\r\n");
            File.AppendAllText(filename, evt.BACKGROUND_NAME + "\r\n");
        }

    }
}
