﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HHHSR_Story_Editor.CanvasObjects
{
    public abstract class CanvasElement
    {
        public enum Type
        {
            Generic,
            Shape,
            Image
        }

        public string name;

        // top left corner :
        public float x;
        public float y;

        // dimensions : 
        public float width;
        public float height;
        

        public Type type;

        public CanvasElement(string name, float x, float y, float width, float height, Type type)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.type = type;
        }


        public bool IsCrossing(float xC , float yC)
        {
            if(xC > this.x && xC < (this.x + width))
            {
                if (yC > this.y && yC < (this.y + height))
                {
                    return true;   
                }
            }
            return false;
        }

        public void SetPosition(float xPos, float yPos)
        {
            x = xPos;
            y = yPos;
        }

    }
}
