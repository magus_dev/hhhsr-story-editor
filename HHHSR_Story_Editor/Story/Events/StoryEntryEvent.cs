﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HHHSR_Story_Editor.Story.Events
{
    public class StoryEntryEvent : StoryEvent
    {

        public static StoryEntryEvent Create(List<string> storyDatas, ulong ID)
        {
            if(storyDatas == null || storyDatas.Count < 1)
            {
                return null;
            }

            StoryEntryEvent evt = new StoryEntryEvent();

            ulong NEXT_ID = ulong.MaxValue;
            try
            {
                if(!ulong.TryParse(storyDatas[0], out NEXT_ID)){
                    return null;
                }
            }catch(Exception ex)
            {
                Console.WriteLine("ERROR: Failed to parse NEXT_ID in StoryEntryEvent");
            }

            evt.ID = ID;
            evt.NEXT_ID = NEXT_ID;
            return evt;

        }

        public override ulong GetNextID()
        {
            return NEXT_ID;
        }
    }
}
