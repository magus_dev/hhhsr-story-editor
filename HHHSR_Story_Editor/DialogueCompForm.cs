﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HHHSR_Story_Editor
{
    public partial class DialogueCompForm : Form
    {

        List<CanvasObjects.CanvasElement> canvasElements;
        float ASPECT_RATIO = 16.0f / 9.0f;
        Point lastMousePos = new Point();
        float absMousePosX;
        float absMousePosY;

        int start_width = 0;
        int start_height = 0;
        int wcur_tmp = 0;
        int hcur_tmp = 0;

        bool movingElement = false;
        int indexMovingElement = -1;
        int lastSelectedElement = -1;

        Font debugFont = new Font("Arial", 10, FontStyle.Bold);

        public DialogueCompForm()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            /*
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            panelDCMain, new object[] { true });
            */

            canvasElements = new List<CanvasObjects.CanvasElement>();
            canvasElements.Add(new CanvasObjects.CanvasImageElement("background", 0, 0, 0, 0, null));
            canvasElements.Add(new CanvasObjects.CanvasImageElement("background2", 0, 0, 0, 0, null));
            canvasElements.Add(new CanvasObjects.CanvasShapeElement("ellipse1", 0.869f, 0.5f, 0.1f, 0.1f, CanvasObjects.CanvasShapeElement.Shape.Ellipse));
            canvasElements.Add(new CanvasObjects.CanvasShapeElement("rectangle1", 0.869f, 0.1f, 0.1f, 0.1f, CanvasObjects.CanvasShapeElement.Shape.Rectangle));
        }

        public void SetBackground(string fullBGFilePath)
        {
            Image bgImage = new Bitmap(fullBGFilePath);
            ASPECT_RATIO = (float)bgImage.Width / (float)bgImage.Height;
            canvasElements[0] = new CanvasObjects.CanvasImageElement("background", 0, 0,1.0f, 1.0f, bgImage);
            CalcNewArea();
        }

        private void DrawMain(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (start_width <= 0) { start_width =   this.panelDCMain.Width; }
            if (start_height <= 0) { start_height = this.panelDCMain.Height; }

            wcur_tmp = this.panelDCMain.Width;
            hcur_tmp = this.panelDCMain.Height;

            float percChangeWidth = (float)wcur_tmp / (float)start_width;
            float percChangeHeight = (float)hcur_tmp / (float)start_height;

            foreach (var el in canvasElements)
            {
                Draw(g, el, wcur_tmp, hcur_tmp);
            }


            DrawDebug(g);
        }

        private void DialogueCompForm_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void DrawDebug(Graphics g)
        {
            absMousePosX = (float)lastMousePos.X / (float)wcur_tmp;
            absMousePosY = (float)lastMousePos.Y / (float)hcur_tmp;

            string dbgString = "Canvas Size : Width :" + this.ClientSize.Width + "  Height : " + this.ClientSize.Height + "" +
                "\r\nLast Mouse Pos : X : " + lastMousePos.X + "  Y : " + lastMousePos.Y + "" +
                "\r\nMouse pos abs : X : " + absMousePosX.ToString("0.000") + "  Y : " + absMousePosY.ToString("0.000") + "" +
                "";
            if (movingElement)
            {
                dbgString += "\r\nMoving element : " + canvasElements[indexMovingElement].name 
                    + "  X : " + canvasElements[indexMovingElement].x.ToString("0.000") 
                    + "  Y : " + canvasElements[indexMovingElement].y.ToString("0.000"); 
            }
            else if(lastSelectedElement > 0 && lastSelectedElement < canvasElements.Count)
            {
                dbgString += "\r\nLast selected element : " + canvasElements[lastSelectedElement].name;
            }

            Brush bgFillBrush = new SolidBrush(Color.FromArgb(100, 0, 0, 0));
            Brush debgStringBrush = Brushes.White;
            
            g.FillRectangle(bgFillBrush, 0, 0, 200, 48);
            g.DrawString(dbgString, debugFont, debgStringBrush, 0, 0);
        }

        private void Draw(Graphics g , CanvasObjects.CanvasElement canvasElement, float w , float h)
        {
            if(g == null || canvasElement == null)
            {
                return;
            }
            if(canvasElement.type == CanvasObjects.CanvasElement.Type.Image)
            {
                CanvasObjects.CanvasImageElement c = (CanvasObjects.CanvasImageElement)canvasElement;
                if(c.image == null)
                {
                    return;
                }
                g.DrawImage(c.image, c.x * w, c.y * h, c.width * w, c.height * h);
            }
            else if(canvasElement.type == CanvasObjects.CanvasElement.Type.Shape)
            {
                CanvasObjects.CanvasShapeElement s = (CanvasObjects.CanvasShapeElement)canvasElement;
                if(s.shape == CanvasObjects.CanvasShapeElement.Shape.Ellipse)
                {
                    g.DrawEllipse(Pens.White, s.x * w, s.y * h, (s.width * w), (s.height * h));
                }else if(s.shape == CanvasObjects.CanvasShapeElement.Shape.Rectangle)
                {
                    g.DrawRectangle(Pens.White, s.x * w, s.y * h, s.width * w, s.height * h);
                }
            }
        }
        
        private void CheckCollisionWithElements(Point p)
        {
            absMousePosX = (float)p.X / (float)wcur_tmp;
            absMousePosY = (float)p.Y / (float)hcur_tmp;
            // skip 0 because its bg
            int index = 1;
            bool foundCrossingElement = false;
            for (; index < canvasElements.Count; index++)
            {
                // TODO 
                if(canvasElements[index].IsCrossing(absMousePosX , absMousePosY))
                {
                    foundCrossingElement = true;
                    break;
                }
                else
                {
                    continue;
                }
            }
            if (foundCrossingElement)
            {
                if(index <= canvasElements.Count)
                {
                    CanvasObjects.CanvasElement canvasElement = canvasElements[index];
                    //MessageBox.Show("Element found! at index : " + index + " " + canvasElement);
                    indexMovingElement = index;
                    lastSelectedElement = indexMovingElement;
                    movingElement = true;
                }
            }
            else
            {
                lastSelectedElement = -1;
                indexMovingElement = -1;
            }
        }
        
        private void MouseDown(MouseEventArgs e)
        {
            Point p = e.Location;
            CheckCollisionWithElements(p);
        }
        private void MouseUp(MouseEventArgs e)
        {
            if (movingElement)
            {
                if (indexMovingElement > 0 && indexMovingElement < canvasElements.Count)
                {
                    bool changeMade = false;
                    if (canvasElements[indexMovingElement].x > 0.99f)
                    {
                        canvasElements[indexMovingElement].x = 0.99f;
                        changeMade = true;
                    }
                    else if (canvasElements[indexMovingElement].x + canvasElements[indexMovingElement].width < 0.01f)
                    {
                        canvasElements[indexMovingElement].x = 0.01f - canvasElements[indexMovingElement].width;
                        changeMade = true;
                    }
                    if (canvasElements[indexMovingElement].y > 0.99f)
                    {
                        canvasElements[indexMovingElement].y = 0.99f;
                        changeMade = true;
                    }
                    else if (canvasElements[indexMovingElement].y + canvasElements[indexMovingElement].height < 0.01f)
                    {
                        canvasElements[indexMovingElement].y = 0.01f - canvasElements[indexMovingElement].height;
                        changeMade = true;
                    }
                    if (changeMade)
                    {
                        this.Refresh();
                    }
                }
            }
            movingElement = false;
        }
        private void MouseMove(MouseEventArgs e)
        {
            lastMousePos = e.Location;

            if (movingElement)
            {
                if (indexMovingElement >= 0)
                {
                    canvasElements[indexMovingElement].SetPosition(absMousePosX, absMousePosY);
                }
            }


            this.Refresh();
        }
        private void MouseLeave(EventArgs e)
        {
            movingElement = false;
        }



        private void panelDCMain_Paint(object sender, PaintEventArgs e)
        {
            DrawMain(e);
        }

        private void DialogueCompForm_Resize(object sender, EventArgs e)
        {
            CalcNewArea();
        }

        private void CalcNewArea()
        {
            Size parentSize = panelDCMain.Parent.ClientSize;
            int newWidth = parentSize.Width;
            int newHeight = (int)(newWidth / ASPECT_RATIO);
            if (newHeight > parentSize.Height)
            {
                newHeight = parentSize.Height;
                newWidth = (int)(newHeight * ASPECT_RATIO);
            }
            panelDCMain.Size = new Size(newWidth, newHeight);
            this.Refresh();
        }

        private void panelDCMain_MouseDown(object sender, MouseEventArgs e)
        {
            MouseDown(e);
        }
        private void panelDCMain_MouseUp(object sender, MouseEventArgs e)
        {
            MouseUp(e);
        }
        private void panelDCMain_MouseMove(object sender, MouseEventArgs e)
        {
            MouseMove(e);
        }
        private void panelDCMain_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(e);
        }

    }
}
