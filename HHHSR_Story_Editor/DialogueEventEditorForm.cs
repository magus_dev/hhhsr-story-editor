﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HHHSR_Story_Editor.Story.Events;

namespace HHHSR_Story_Editor
{
    public partial class DialogueEventEditorForm : Form
    {
        string fullBGFilePath;
        string fileNameWithoutExtension;

        Action<DialogueEvent> onChangeDialogue;
        DialogueEvent thisEvent;

        DialogueCompForm dialogueCompForm;


        public DialogueEventEditorForm()
        {
            InitializeComponent();
        }

        public static void SetData(DialogueEventEditorForm eventEditorForm, DialogueEvent evt, Action<DialogueEvent> onChangeDialogue)
        {
            if(evt == null || eventEditorForm == null || onChangeDialogue == null)
            {
                return;
            }
            // set comp form
            if (eventEditorForm.dialogueCompForm != null)
            {
                eventEditorForm.dialogueCompForm.Dispose();
                eventEditorForm.dialogueCompForm = null;
            }
            eventEditorForm.dialogueCompForm = new DialogueCompForm();
            eventEditorForm.dialogueCompForm.TopLevel = false;
            eventEditorForm.panelEditorCompPlacement.Controls.Add(eventEditorForm.dialogueCompForm);
            eventEditorForm.dialogueCompForm.FormBorderStyle = FormBorderStyle.None;
            eventEditorForm.dialogueCompForm.Dock = DockStyle.Fill;
            eventEditorForm.dialogueCompForm.Show();

            eventEditorForm.thisEvent = evt;
            // Set action 
            eventEditorForm.onChangeDialogue = onChangeDialogue;
            // Set id text
            eventEditorForm.textBoxEventID.Text = evt.ID.ToString();
            eventEditorForm.textBoxNextEventID.Text = evt.NEXT_ID.ToString();
            // Set dialogue text
            eventEditorForm.textBoxDialogue.Text = evt.TEXT;
            // Set image
            eventEditorForm.fullBGFilePath = Path.Combine(MainForm.storyRootFolder, "backgrounds", evt.BACKGROUND_NAME + ".png");
            eventEditorForm.SetBGImage(true);


        }

        // Image button
        private void buttonOpenBackground_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog.InitialDirectory = MainForm.lastFolderOpened;
            DialogResult result = openFileDialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                fullBGFilePath = openFileDialog.FileName;
                SetBGImage();
            }
        }
        // Sets the bg image and filename
        private void SetBGImage(bool isOnFormLoad = false)
        {
            if (!string.IsNullOrEmpty(fullBGFilePath))
            {
                fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fullBGFilePath);
                if (File.Exists(fullBGFilePath))
                {
                    pictureBoxBackground.Image = new Bitmap(fullBGFilePath);
                    dialogueCompForm.SetBackground(fullBGFilePath);
                }
            }
            else
            {
                fileNameWithoutExtension = "";
            }

            thisEvent.BACKGROUND_NAME = fileNameWithoutExtension;

            if (!isOnFormLoad)
            {
                onChangeDialogue(thisEvent);
            }
        }

        private void buttonGotoNextStoryEvent_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.GotoNextEvent(ulong.Parse(this.textBoxNextEventID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("The next ID does not exist or could not be parsed!");
            }
        }

        private void textBoxDialogue_TextChanged(object sender, EventArgs e)
        {
            thisEvent.TEXT = textBoxDialogue.Text;
            onChangeDialogue(thisEvent);
        }

        private void buttonGotoPreviousEvent_Click(object sender, EventArgs e)
        {
            MainForm.GotoPreviousEvent();
        }
    }
}
